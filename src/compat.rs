/// Get the OLE automation epoch as a UNIX timestamp
fn get_oadate_unix() -> std::time::SystemTime {
    let epoch = std::time::UNIX_EPOCH;
    epoch - std::time::Duration::from_secs(2209161600)
}

/// Convert from and to OLE automation date
pub trait OLEAutomationDateCompatibility {
    /// Create a date from OLE Automation date
    fn from(oadate: f64) -> Self;
    /// Convert to OLE Automation date
    fn to_oadate(&self) -> f64;
}

impl OLEAutomationDateCompatibility for std::time::SystemTime {
    fn from(oadate: f64) -> Self {
        let epoch_secs = oadate * 24.0 * 3600.0;
        get_oadate_unix() + std::time::Duration::from_secs(epoch_secs as u64)
    }

    fn to_oadate(&self) -> f64 {
        let span = if let Ok(span) = self.duration_since(get_oadate_unix()) {
            span
        } else {
            get_oadate_unix().duration_since(*self).unwrap()
        };
        span.as_secs_f64() / 24.0 / 3600.0
    }
}

#[cfg(test)]
mod test {
    use super::OLEAutomationDateCompatibility;

    #[test]
    fn test_stable_conversion() {
        let time = std::time::UNIX_EPOCH;
        let oadate = time.to_oadate();
        let converted: std::time::SystemTime = OLEAutomationDateCompatibility::from(oadate);
        assert_eq!(time, converted);
    }
}
