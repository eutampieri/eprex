/// WIP DEFLATE decompressing lazy iterator
pub struct Deflating<T: Iterator<Item = u8>> {
    /// DEFLATE decoder
    stream: inflate::InflateStream,
    /// decompresseed bytes
    out_buffer: Vec<u8>,
    /// last returned byte in out_buffer
    out_buffer_pos: usize,
    /// compressed bytes iterator
    in_iter: T,
    /// total decompressed size
    cursor: usize,
}

impl<T: Iterator<Item = u8>> Iterator for Deflating<T> {
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item> {
        if self.out_buffer_pos < self.out_buffer.len() - 1 {
            self.out_buffer_pos += 1;
            Some(self.out_buffer[self.out_buffer_pos])
        } else {
            match self.in_iter.next() {
                Some(byte) => match self.stream.update(&[byte]) {
                    Ok((read, buf)) => {
                        self.out_buffer_pos = 0;
                        self.out_buffer = buf.to_vec();
                        self.cursor += read;
                        Some(self.out_buffer[self.out_buffer_pos])
                    }
                    Err(_) => None,
                },
                None => None,
            }
        }
    }
}
