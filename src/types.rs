use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct DayFlags {
    /// This fragment should appear in every page
    pub nan: bool,
    pub biennale: bool,
    pub gloria: bool,
    /// Ufficio
    pub ufficio: bool,
    /// Lodi
    pub lodi: bool,
    /// Terza
    pub terza: bool,
    /// Sesta
    pub sesta: bool,
    /// Nona
    pub nona: bool,
    /// Vespri
    pub vespri: bool,
    /// Compieta
    pub compieta: bool,
    /// Liturgia
    pub liturgia: bool,
    /// Saints of the current day
    pub santi: bool,
    /// Ufficio dei defunti
    pub defunti: bool,
}

impl From<u32> for DayFlags {
    fn from(n: u32) -> Self {
        Self {
            nan: n == 0,
            biennale: n & 512 != 0,
            gloria: n & 1024 != 0,
            ufficio: n & 2 != 0,
            lodi: n & 4 != 0,
            terza: n & 8 != 0,
            sesta: n & 16 != 0,
            nona: n & 32 != 0,
            vespri: n & 64 != 0,
            compieta: n & 128 != 0,
            liturgia: n & 256 != 0,
            santi: n & 2048 != 0,
            defunti: n & 4096 != 0,
        }
    }
}
#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub enum DayPart {
    /// Ufficio
    Ufficio,
    /// Lodi
    Lodi,
    /// Terza
    Terza,
    /// Sesta
    Sesta,
    /// Nona
    Nona,
    /// Vespri
    Vespri,
    /// Compieta
    Compieta,
    /// Liturgia
    Liturgia,
    /// Saints of the current day
    Santi,
    /// Ufficio dei defunti
    Defunti,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Record {
    Text {
        /// Unique ID across data store
        id: u32,
        /// Text fragment in HTML
        text: String,
    },
    /// This is currently unused
    Hour {
        /// Unique ID across data store
        id: u32,
        name: String,
    },
    /// This is a reference to the various fragments for each day and for each lithurgical time
    Day {
        date: std::time::SystemTime,
        /// [fragments.0] is the text record ID, [fragments.1] is the lithurgical time and [fragments.2] is an additional flags store
        fragments: Vec<(u32, DayFlags)>,
    },
    /// Database version, usually the first record
    Version {
        /// Unique ID across data store
        id: u32,
        /// Version string
        version: String,
    },
}

#[cfg(test)]
mod tests {
    //use crate::decoding::RecordIterator;

    /*#[test]
    fn test_gloria() {
        let bytes = std::fs::read("eprex.dat").unwrap();
        let bytes = inflate::inflate_bytes(&bytes).unwrap();
        std::fs::write("unzipped", bytes.clone());
        let mut iter: RecordIterator<_> = bytes.into_iter().into();
        let record = iter
            .filter_map(|x| match x {
                super::Record::Day { date, fragments } => Some((
                    date.duration_since(std::time::UNIX_EPOCH)
                        .unwrap()
                        .as_secs(),
                    fragments,
                )),
                _ => None,
            })
            .find(|x| x.0 == 1639872000)
            .map(|x| x.1)
            .unwrap();
        let gloria = record
            .into_iter()
            .filter(|x| match &x.1 {
                Some(x) => match x {
                    crate::types::DayPart::Liturgia => true,
                    _ => false,
                },
                None => false,
            })
            .map(|x| dbg!(x))
            .fold(false, |acc, x| acc || (x.2).gloria);
        assert!(!gloria);
    }*/
}
