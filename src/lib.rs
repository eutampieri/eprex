/// Date compatibility functions
pub mod compat;
/// Uncompressed ePrex iterator decoder
pub mod decoding;
/// DEFLATE decompressing iterator (WIP)
pub mod iters;
/// ePrex types
pub mod types;
