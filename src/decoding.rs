use crate::{
    compat::OLEAutomationDateCompatibility,
    types::{DayFlags, DayPart, Record},
};

/// Read LE i32 from bytes iterator
fn read_i32<I: Iterator<Item = u8>>(iter: &mut I) -> i32 {
    i32::from_le_bytes([
        iter.next().unwrap(),
        iter.next().unwrap(),
        iter.next().unwrap(),
        iter.next().unwrap(),
    ])
}
/// Read LE u32 from bytes iterator
fn read_u32<I: Iterator<Item = u8>>(iter: &mut I) -> u32 {
    u32::from_le_bytes([
        iter.next().unwrap(),
        iter.next().unwrap(),
        iter.next().unwrap(),
        iter.next().unwrap(),
    ])
}
/// Read string from bytes iterator
fn read_string<I: Iterator<Item = u8>>(iter: &mut I) -> String {
    let len = read_i32(iter) as usize;
    let bytes = iter.take(len).collect::<Vec<_>>();
    String::from_utf8_lossy(&bytes).to_string()
}

/// Iterator over ePrex (decompressed) dat file. Yields [crate::types::Record]s
pub struct RecordIterator<T: Iterator<Item = u8>> {
    /// Bytes iterator from which we extract the records
    in_iter: T,
}
impl<T: Iterator<Item = u8>> Iterator for RecordIterator<T> {
    type Item = crate::types::Record;

    fn next(&mut self) -> Option<Self::Item> {
        let record_type_id = self.in_iter.next()?;
        let record_id = read_u32(&mut self.in_iter);
        match record_type_id {
            0 => Some(Record::Text {
                id: record_id,
                text: read_string(&mut self.in_iter),
            }),
            1 => Some(Record::Hour {
                id: record_id,
                name: read_string(&mut self.in_iter),
            }),
            2 => {
                let frags = read_string(&mut self.in_iter)
                    .split(';')
                    .filter_map(|x| {
                        let f = x.split('|').collect::<Vec<_>>();
                        if f.len() >= 2 {
                            Some((
                                f[1].parse::<u32>().unwrap_or_default(),
                                DayFlags::from(f[0].parse::<u32>().unwrap_or_default()),
                            ))
                        } else {
                            None
                        }
                    })
                    .collect();
                Some(Record::Day {
                    date: OLEAutomationDateCompatibility::from(record_id as f64),
                    fragments: frags,
                })
            }
            3 => Some(Record::Version {
                id: record_id,
                version: read_string(&mut self.in_iter),
            }),
            _ => None,
        }
    }
}

impl<T: Iterator<Item = u8>> RecordIterator<T> {
    /// Return two dicts, the first having a timestamp as a key and the [crate::types::Record::Day] as a value,
    /// the latter contains text fragments with their IDs a key.
    pub fn fold_to_dicts(
        &mut self,
    ) -> (
        std::collections::HashMap<u64, Vec<(u32, DayFlags)>>,
        std::collections::HashMap<u32, String>,
    ) {
        self.fold(
            (
                std::collections::HashMap::new(),
                std::collections::HashMap::new(),
            ),
            |(mut frags, mut texts), x| {
                match x {
                    Record::Text { id, text } => {
                        texts.insert(id, text);
                    }
                    Record::Day { date, fragments } => {
                        frags.insert(
                            date.duration_since(std::time::UNIX_EPOCH)
                                .unwrap()
                                .as_secs(),
                            fragments,
                        );
                    }
                    _ => {}
                };
                (frags, texts)
            },
        )
    }
}

impl<T> From<T> for RecordIterator<T>
where
    T: Iterator<Item = u8>,
{
    fn from(iter: T) -> Self {
        RecordIterator { in_iter: iter }
    }
}
